                ! Definicion de modulus
        module Iniciar_tipos_derivados_y_funciones

        public
        type matSpar
                real :: k
                real :: l
                real :: d
        endtype matSpar

        !-------------
        ! Interfaces
        !------------
        interface operator(*) !Sobrecarga del operador * para ambos casos
                module procedure matSparXVec, matSparXmat
        end interface

        !---------
        !Funciones
        !---------
        !Definicion de subproramas de modulo o "procedi:miento de modulo"
        contains
       pure function matSparXVec(b, SPAR) result(RES)
        !       implicit none
                type(matSpar), intent(in)               ::SPAR
        !       real, intent(in)                        ::k, l
                real, intent(in), dimension(:)          ::b
                real, dimension(size(b))                ::RES
        !       real, pointer                           ::RES(:)
                integer                                 ::ierr, i

        !        allocate(RES(size(b)))

        !Calculo
                RES =   (/&
                        SPAR%k*b(1)+SPAR%l*b(2), &
                        (SPAR%l*b(i-1)+SPAR%k*b(i)+SPAR%l*b(i+1),i=2,size(b)-1),& 
                        SPAR%l*b(size(b)-1)+SPAR%k*b(size(b))&
                        /)

        endfunction matSparXVec
        !
        !------------------------------------------------------------------------------------
        !------------------------------------------------------------------------------------
        !
pure function matSparXMat(SPAR,B) result(RES)
!       implicit none
        type(matSpar), intent(in)                       :: SPAR
!       real, intent(in)                                ::k, l
        real, intent(in), dimension(:,:)                ::B
        real,dimension(size(B,1),size(B,2))             ::RES

        
!        allocate(RES(size(B(:,1)),1))        

!       call  matsparXVec(k, l, B(1, :), R(1, :))
        RES(1,:)=B(1,:)*SPAR+B(2,:) 

!        print *, '------INCODE1------'
!        print *, B(1,:)
!        print *, ' '
!        print *, RES(1,:)
!        print *, '------INCODE1------'
!
        forall(i = 2:(size(RES,1)-1))
                RES(i,:)= B(i-1,:)+ B(i,:)*SPAR+B(i+1,:)

!        print *, '------INCODE',i,'------'
!        print *, B(i,:)
!        print *, ' '
!        print *, RES(i,:)
!        print *, '------INCODE',i,'------'
        endforall

!       call matsparXVec(k,l,B(size(B(:,1)),:),R(size(R(:,1)),:))

        RES(size(RES,1),:)=B(size(B,1),:)*SPAR+B(size(B,1)-1,:)

endfunction matSparXmat

function invertM(M) result(Minv)
        use GaussJordan

        implicit none

        real, allocatable, dimension(:,:), intent(in)   :: M
        real, allocatable, dimension(:,:)               :: Minv, aux
        integer                                         :: n, i

        n=size(M,1)
        i=0

        allocate(Minv(n,n),aux(n,n))

        aux=M
        Minv=0.0
        forall(i=1:n) Minv(i,i)=1.0

        call GJSolver(aux,Minv)

endfunction invertM

endmodule  Iniciar_tipos_derivados_y_funciones

!----------------------------------------------
!     Modulo de CG vectorial
!----------------------------------------------

module CG
use Iniciar_tipos_derivados_y_funciones

contains
function CGV(SPAR, b) result(X_0)
        integer                         :: CGi
        real, dimension(:), allocatable :: Ax_0
        real, dimension(:), allocatable :: Ad
        real, dimension(:), allocatable :: r
        real, dimension(:), allocatable :: r1
        real, dimension(:), allocatable :: d
        real, dimension(:), allocatable :: X_0
!       real, dimension(:), allocatable :: CGV
        real                            :: alpha
        real                            :: beta
        type(MatSpar), intent(in)       :: SPAR
        real, dimension(:),intent(in)   :: b

        CGi=0
        alpha=0.0
        beta=100.0
        allocate(X_0(size(b)), r(size(b)), d(size(b)), r1(size(b)))
        allocate(Ax_0(size(b)), Ad(size(b)))

        r=b-Ax_0
        d=r

iteracion:      do, CGi=0,size(b)
                        Ad=d*SPAR

                        alpha=dot_product(r,r)/dot_product(r,Ad)

                        X_0=X_0+alpha*d

                        r1=r-alpha*Ad

                        beta=dot_product(r1,r1)/dot_product(r,r)

                        r=r1

                        d=r1+beta*d

                        if(beta<0.000000001) exit iteracion !Condicion de salida por precision
                enddo iteracion

!CGV=X_0

endfunction CGV


!Resolucion de Gradientes conjugados matricial!
!NOTA: EL CODIGO ES UNA EXTRAPOLACION DEL DE VECTORES (QUE SI ANDA)
!ESTO NO IMPLICA POR NINGUN MOTIVO QUE EL RESULTADO ENTREGADO DEL CODIGO A CONTINUACON
! SEA CORRECTO
!
function CGM(SPAR,BB) result(XX_0)     

        integer                              :: CGi, n
        real, dimension(:,:), allocatable    :: Ad, r, r1, d, XX_0, ALPHA, BETA, aux
        type(MatSpar), intent(in)            :: SPAR
        real, dimension(:,:),intent(in)      :: BB

        n=size(BB,1)
        CGi=0
    
        allocate(Ad(n,n), r(n,n), r1(n,n), d(n,n), XX_0(n,n), ALPHA(n,n), BETA(n,n), aux(n,n))


        ALPHA=0.0
        BETA=1000.0
        XX_0=0.0

        r=BB
        d=BB

iteracion:      do, CGi=0,n
                        Ad=SPAR*BB

                        aux=matmul(r,Ad)
                        aux=invertM(aux)
                        ALPHA=matmul(matmul(r,r), aux) !Esta linea hace cualquier cosa, deberia invertir la segunda matriz.

                        XX_0=X_00+matmul(alpha,d)

                        r1=r-matmul(alpha,Ad)

                        aux=matmul(r,r)
                        aux=invertM(aux)
                        beta=matmul(matmul(r1,r1),aux) !Esta linea tambien hace cualquier cosa por el mismo motivo. La division no deberia ser elemento por elemento sino entre matrices

                        r=r1

                        d=r1+matmul(beta,d)

                        if(sum(beta)<0.00000001) exit iteracion !Condicion de salida por precision
                enddo iteracion

endfunction CGM

endmodule CG



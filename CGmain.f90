!---------------------------------------------------
!               Inicio de programa principal
!--------------------------------------------------

program CGmain
!       Solver para problemas implicitos por gradientes conjugados
!                          Alfonso Santiago

use Iniciar_tipos_derivados_y_funciones
use CG

        implicit none

        real, dimension(:), allocatable         ::b ! "b" es el vector a resolver 
        real, pointer                           ::r(:) ! "r" donde se alojara el resultado
        real, dimension(:,:), allocatable       ::BB, RR ! "BB" y "RR" son matrices a resolver y resultado respectivamente
        integer                                 ::i, j, ierr, n, m
!       real, parameter                         ::k = 4.0, l = 1.0
        type(matspar)                           ::MatLight
        
        MatLight=matspar(4.0,1.0,3)
        ! MatLight%k=4.0
        ! MatLight%l=1.0
        ! MatLight%d=10.0

        n=4 !Dimension vector
        m=4 !Dimension del segundo orden

        allocate(r(n), b(n), RR(n,m), BB(n,m))! stat=ierr) 
        allocate(b(n), stat=ierr)

        !allocate(RR(n,m), stat=ierr)
        !allocate(BB(n,m), stat=ierr)

        b=[(i, i = 1, size(b))]
        r=0! [(0, i = 1, size(r))]
        
        i=0.0
        j=0.0

        BB=0.0
        do concurrent  (i = 1:size(BB,1), j=1:size(BB,2))
            BB(i,j)=i+j
        enddo 

        RR(i,j) = 0 
   
!CODIGO DE PRUEBA PARA LA MULTIPLICACION DE VECTORES
!
!        print *, 'vector a multiplicar:'
!        print *, b(:)
!        r=b*MatLight
!        print *, 'vector a multiplicar luego de la multiplicacion:'
!        print *, b(:)
!        print *, 'vector resultado:'
!        print *, r(:)
!
!FIN CODIGO DE PRUEBA MULTIPLICACION DE VECTORES

! CODIGO DE PRUEBA DE MULTIPLICACION DE MATRICES
!
!       print *, 'matriz a multiplicar'
!       do i=1,size(BB,1)
!               print *, BB(:,i)
!       enddo


!       RR=MatLight*BB


!       print *, 'matriz a multiplicar luego del codigo'
!       do i=1,size(BB,1)
!               print *, BB(:,i)
!       enddo

!       print *, 'matriz resultado'
!       do i=1,size(RR,1)
!               print *, RR(:,i)
!       enddo 

!
!FIN CODIGO DE PRUEBA DE MULTIPLICACION DE MATRICES

!CODIGO DE PRUEBA DE GRADIENTES CONJUGADOS PARA VECTORES
!

!        print *, 'vector a resolver:'
!        print *, b(:)
!        r=CGV(MatLight,b)
!        print *, 'vector a resolver luego de la multiplicacion:'
!        print *, b(:)
!        print *, 'vector resultado:'
!        print *, r(:)

!
!FIN CODIGO DE PRUEBA DE GRADIENTES CONJUGADOS PARA VECTORES

!CODIGO DE GRADIENTES CONJUGADOS PARA MATRICES
!NOTA: EL CODIGO ES UNA EXTRAPOLACION DEL DE VECTORES (QUE SI ANDA)
!ESTO NO IMPLICA POR NINGUN MOTIVO QUE EL RESULTADO ENTREGADO SEA CORRECTO
!

       print *, 'matriz a resolver'
       do i=1,size(BB,1)
               print *, BB(:,i)
       enddo


       RR=CGM(MatLight,BB)


       print *, 'matriz a multiplicar luego del codigo'
       do i=1,size(BB,1)
               print *, BB(:,i)
       enddo

       print *, 'matriz resultado'
       do i=1,size(RR,1)
               print *, RR(:,i)
       enddo 

!
!FIN CODIGO
end program CGmain

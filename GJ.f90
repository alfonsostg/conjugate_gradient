module GaussJordan
    implicit none

contains
    subroutine GJSolver(A,B)
        use nrtype
        implicit none
        real(SP), intent(inout), dimension(:,:)     :: A, B
        real(SP)                                    :: raux(size(A,1)), ac(size(B,2))
        real(SP)                                    :: aii, aji
        integer(I2B)                                :: i, j, k, nA, niB, njB, rmax, p



        if((size(A,1).ne.size(A,2)).or.size(A,1).ne.size(B,1)) then
            stop 'matriz no cuadrada o matrices Aij y Bjk de distinto tamanio'
        endif

        nA=size(A,1); niB=size(B,1); njB=size(B,2)

        do i=1,nA
            aii=A(i,i)
            if(aii.eq.0) then 
                 raux= maxloc(A(i:nA,i))
                 rmax=raux(1)

               ! print *, 'antes del pivot'
               ! do k=1,size(A,1)
               !    print *, A(k,:), '|', B(k,1)
               ! enddo

                 raux(:)=A(i,:)
                 A(i,:)=A(rmax,:)
                 A(rmax,:)=raux(:)

                 raux(:)=B(i,:)
                 B(i,:)=B(rmax,:)
                 B(rmax,:)=raux(:)

                ! print *, 'despues del pivot'
                ! do k=1,size(A,1)
                !     print *, A(k,:), '|', B(k,1)
                ! enddo
                 
                 aii=A(i,i)
            endif

            A(i,:)=A(i,:)/aii
            B(i,:)=B(i,:)/aii

            do j=i+1,nA
                aji=A(j,i)
                if(aji.eq.0) cycle
                A(j,:)=A(j,:)-A(i,:)*aji
                B(j,:)=B(j,:)-B(i,:)*aji
            enddo

        enddo

! Code for backsubstitution

if(.false.) then
        do i=nA,1,-1
            aii=A(i,i)

            do j=i-1,1,-1
                aji=A(j,i)
                if(aji.eq.0) cycle
            A(j,:)=A(j,:)-aji*A(i,:)
            B(j,:)=B(j,:)-aji*B(i,:)
            enddo
        enddo
else
    !B(nA,:)=B(nA,:)/A(nA,nA)

    !B(nA-1,:)=(B(nA-1,:)- B(nA-1+1,:)*A(nA-1,nA))/A(nA-1,nA-1)

    !...
   ! do i=nA,1,-1
   !     ac=0.0
   !    !print *, i
   !     do j=i+1,nA
   !         ac=ac+A(i,j)*B(j,:)
   !        ! print *, i, j, ac
   !     enddo

   !    B(i,:)=(B(i,:)-ac)/A(i,i)
   ! enddo

    do j=1,size(B,2)

        do i=nA,1,-1
            B(i,j)=(B(i,j)-sum(A(i,i+1:nA)*B(i+1:nA,j)))/A(i,i)
        enddo
    enddo

    where(A.ne.1) A=0.0

!    forall(i=nA:1:-1,j=i+1:nA-1)
!        B(i,:)=(B(i,:)-sum(A(i,j)*B(j,:)))/A(i,i)
!    endforall

endif

    endsubroutine GJSolver

endmodule GaussJordan
